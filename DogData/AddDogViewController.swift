//
//  AddDogViewController.swift
//  DogData
//
//  Created by Carl on 2016/1/8.
//  Copyright © 2016年 Carl Lu. All rights reserved.
//

import UIKit
import RealmSwift

class AddDogViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {

    @IBOutlet weak var picker: UIPickerView!
    
    @IBOutlet weak var nameTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.picker.dataSource = self
        self.picker.delegate = self
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 21
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(row)"
    }

    @IBAction func saveTapped(sender: AnyObject) {
        let dogName = self.nameTextField.text!
        let dogAge = self.picker.selectedRowInComponent(0)
        
        let dog = Dog()
        dog.name = dogName
        dog.age = dogAge
        
        do {
            let realm = try Realm()
            try realm.write {
                realm.add(dog)
            }
        } catch _ {
        }
        self.navigationController?.popViewControllerAnimated(true)
    }
    
}
