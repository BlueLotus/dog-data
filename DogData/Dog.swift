//
//  Dog.swift
//  DogData
//
//  Created by Carl on 2016/1/8.
//  Copyright © 2016年 Carl Lu. All rights reserved.
//

import Foundation
import RealmSwift

class Dog : Object {
    
    dynamic var name = ""
    dynamic var age = 0
    
}
